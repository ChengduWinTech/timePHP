<?php
/**
 * @author     Bililovy<1985277517@qq.com>
 * @copyright  TimePHP
 * @license    https://github.com/qq1985277517/timePHP
 *  */
// 加载基础文件
require __DIR__ . '/base.php';
// 执行应用
lib\App::run();
?>  