<?php
/**
 * 入口
 * @author     Bililovy<1985277517@qq.com>
 * @copyright  TimePHP
 * @license    https://github.com/qq1985277517/timePHP
 *  */
define('APP_PATH', __DIR__);
// 加载框架引导文件
require __DIR__ . '/timephp/start.php';
?>  